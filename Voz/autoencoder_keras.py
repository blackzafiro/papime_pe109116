from keras.layers import Input,Dense,Activation
from keras.layers.core import Dropout
from keras.models import Model,load_model
from keras import optimizers
import keras.backend as K
from keras.callbacks import ModelCheckpoint
from keras.callbacks import TensorBoard
import numpy as np
import os.path
#encoding_dim=338
#https://1drv.ms/f/s!Arh4odGRXATB6QtibkyePUci5JVM
ruta="kerasModel-{epoch:02d}-{val_acc:.2f}.hdf5"
nombre_modelo="kerasModel.5h"
logs="./logsKeras"
drop=0.70

neuronas=800
#1-8 .15
#8-18 .3
#18-26 .35
#26 -  
origen="/media/archivos/Audio/"
entrenamiento=np.load(origen+"entrenamiento.npy")
entrenamiento_etiquetas=np.load(origen+"entrenamientoEtiquetas.npy")
validacion=np.load(origen+"validacion.npy")
validacion_etiquetas=np.load(origen+"validacionEtiquetas.npy")
prueba=np.load(origen+"prueba.npy")
prueba_etiquetas=np.load(origen+"pruebaEtiquetas.npy")


def ejecutar(autoencoder,iteraciones,iter_guardar):
    checkpoint = ModelCheckpoint(ruta, monitor='val_acc', verbose=0, save_best_only=True, mode='max',period=1)
    cb_tensorboard=TensorBoard(log_dir=logs,histogram_freq=1,write_graph=True,write_images="False")
    opt = optimizers.SGD(lr=0.05, decay=1e-6, momentum=.95, nesterov=False)
    #opt=optimizers.Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=1e-08, schedule_decay=0.004)
    autoencoder.compile(optimizer=opt,loss="mean_squared_error",metrics=["accuracy"])
    
    autoencoder.fit(entrenamiento,entrenamiento_etiquetas,
        epochs=iteraciones,
        batch_size=1,
        validation_data=(validacion,validacion_etiquetas),
        callbacks=[checkpoint,cb_tensorboard]
        )
    print("Guardando modelo")
    autoencoder.save(nombre_modelo)
        

print("Introduce nombre de modelo a restaurar")
nombre_modelo=raw_input()

if os.path.exists(nombre_modelo):
	autoencoder=load_model(nombre_modelo)
	ejecutar(autoencoder,100,10)

	print("Prueba:",autoencoder.evaluate(prueba, prueba_etiquetas, batch_size=40, verbose=1))

else:

	input_img=Input(shape=(338,))

	encoded=Dense(neuronas,activation='sigmoid')(input_img)
	encoded=Dropout(drop)(encoded)
	encoded=Dense(neuronas,activation='sigmoid')(encoded)
	encoded=Dropout(drop)(encoded)
	encoded=Dense(neuronas,activation='sigmoid')(encoded)
	encoded=Dropout(drop)(encoded)

	decoded=Dense(neuronas,activation='sigmoid')(encoded)
	encoded=Dropout(drop)(decoded)
	decoded=Dense(neuronas,activation='sigmoid')(decoded)
	encoded=Dropout(drop)(decoded)
	decoded=Dense(neuronas,activation='sigmoid')(decoded)
	encoded=Dropout(drop)(decoded)
	decoded=Dense(72,activation='softmax')(decoded)	
	
	autoencoder=Model(input_img,decoded)
	
	ejecutar(autoencoder,30,10)

	print("Prueba:",autoencoder.evaluate(prueba, prueba_etiquetas, batch_size=40, verbose=1))
