#--coding:utf-8---
#Autor: Benjamin Torres Saavedra
#Version 1 de Colector
import sys
import os
import cPickle
import os.path

class Colector(object):
	
	@staticmethod
	def find(name, path):
		print("Buscando:",name)
		for root, dirs, files in os.walk(path):
			if name in files:
				return os.path.join(root, name)
	@staticmethod
	def encontrarRutaArchivo(archivoEntrada,prefijo,salida,raiz):
		'''Lee archivoEntrada y lee linea por linea el archivo de texto y su correspondiente etiqueta
		al final crea un archivo con (nombreArchivo,rutaArchivo,etiquetaArchivo,rutaEtiqueta) por cada linea''' 
		#salida=open()archivoSalida,"w")

		with open(archivoEntrada,"r") as entrada: 
			for linea in entrada:
				linea=linea.split()#linea[0] es el nombre del .wav #linea[1] es el nombre del archivo de etiquetas
				ruta=Colector.find(linea[0],raiz)
				ruta2=Colector.find(linea[1],raiz)
				#salida.write(prefijo+" "+linea[0]+" "+find(linea[0],raiz)+" "+linea[1]+" "+find(linea[1],raiz))
				salida.write(prefijo+" "
					+linea[0]
					+" "+ruta
					+" "+linea[1]
					+" "+ruta2+"\n")
	
	@staticmethod
	def encontrarRutas(archivos,prefijos,raiz):
		'''Lee archivoEntrada y lee linea por linea el archivo de texto y su correspondiente etiqueta
		al final crea un archivo con (nombreArchivo,rutaArchivo,etiquetaArchivo,rutaEtiqueta) por cada linea''' 

		if(not len(archivos)==len(prefijos)):
			print("Archivos y prefijos de distinto tamaño")
			return
		else:

			salida=open("archivoResumen","w")
			for i in range(0,len(archivos)):
				Colector.encontrarRutaArchivo(archivos[i],prefijos[i],salida,raiz)
			salida.close()

	def __init__(self,nombre,archivosConjuntos,prefijos,raiz=os.getcwd()):
		self.entrenamiento={}
		self.validacion={}
		self.prueba={}
		self.nombre=nombre
		if(os.path.exists(nombre)):
				self.load()
		else:
			Colector.encontrarRutas(archivosConjuntos,prefijos,raiz)

		with open("archivoResumen",'r') as f:
			for linea in f:
				linea=linea.split()  #(conjunto-archivo-ruta-etiquetas-ruta)
				if(linea[0].upper()=="E"):#entrenamiento
					self.entrenamiento[linea[1]]=(linea[2],linea[3],linea[4])
				elif(linea[0].upper()=="V"):
					self.validacion[linea[1]]=(linea[2],linea[3],linea[4])
				else:
					self.prueba[linea[1]]=(linea[2],linea[3],linea[4])

		self.save()

	def load(self):
		'''http://stackoverflow.com/questions/2709800/how-to-pickle-yourself'''
		f = open(self.nombre,'rb')
		tmp_dict = cPickle.load(f)
		f.close()          
		self.__dict__.update(tmp_dict)

	def save(self):
		f = open(self.nombre,'wb')
		cPickle.dump(self.__dict__,f,2)#2?
		f.close()

	def buscarArchivo(archivo,conjunto="E"):
		try:
			if(conjunto=="E"):#entrenamiento
				return self.entrenamiento[archivo]
			elif(conjunto=="V"):
				return self.validacion[archivo]
			else:
				return self.prueba[archivo]			

		except KeyError:
			print('No existe llave:'+archivo)
			return

	
if __name__=='__main__':
	print("Iniciando...")
	archivos=["entrenamiento.txt","validacion.txt","prueba.txt"]
	prefijos=["E","V","P"]
	colector=Colector("vault",archivos,prefijos)
	print(len(colector.entrenamiento.keys())
		+len(colector.validacion.keys())
		+len(colector.prueba.keys()) )