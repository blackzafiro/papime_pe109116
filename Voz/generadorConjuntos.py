#-.- encoding:utf-8-.-
import sys
import numpy as np
from sklearn.utils import shuffle
import pandas as pd
from recorredor import fonos #importamos la lista de fonos del recorredor


ventanas=np.load(sys.argv[1])
etiquetas=np.load(sys.argv[2])

serie=pd.Series(etiquetas)
etiquetas_onehot=pd.get_dummies(serie).get_values()


prueba=None
pruebaEtiquetas=None
for fono in fonos:
	posiciones=np.argwhere(etiquetas==fono)
	np.random.shuffle(posiciones) #mezclamos el arreglo para obtener posiciones aleatorias ,ya que el listado es de elementos contiguos
	tam=int(posiciones.size*.1) #calculamos el 10% de elementos que concuerdan con el fono
	posiciones=posiciones[0:tam]#recortamos el arreglo de posiciones unicamente al tamaño que usaremos
	if(prueba==None):
		prueba=ventanas[posiciones]
		pruebaEtiquetas=etiquetas_onehot[posiciones]
		#ventanas=np.delete(ventanas,posiciones,0) #eliminamos etiquetas y ventanas de el conjunto original
		#etiquetas=np.delete(etiquetas,posiciones,0)
		etiquetas[posiciones]=0
	else:
		prueba=np.vstack((prueba,ventanas[posiciones]))
		pruebaEtiquetas=np.vstack((pruebaEtiquetas,etiquetas_onehot[posiciones]))
		#ventanas=np.delete(ventanas,posiciones,0) #eliminamos etiquetas y ventanas de el conjunto original
		#etiquetas=np.delete(ventanas,posiciones,0)
		etiquetas[posiciones]=0

entrenamiento=None
entrenamientoEtiquetas=None
for fono in fonos:
	posiciones=np.argwhere(etiquetas==fono)
	np.random.shuffle(posiciones) #mezclamos el arreglo para obtener posiciones aleatorias ,ya que el listado es de elementos contiguos
	tam=int(posiciones.size*.78) #calculamos el 70 de elementos  (del arreglo original) que concuerdan con el fono
	posiciones=posiciones[0:tam]#recortamos el arreglo de posiciones unicamente al tamaño que usaremos
	if(entrenamiento==None):
		entrenamiento=ventanas[posiciones]
		entrenamientoEtiquetas=etiquetas_onehot[posiciones]
		#ventanas=np.delete(ventanas,posiciones,0) #eliminamos etiquetas y ventanas de el conjunto original
		#etiquetas=np.delete(ventanas,posiciones,0)
		etiquetas[posiciones]=0
	else:
		entrenamiento=np.vstack((entrenamiento,ventanas[posiciones]))
		entrenamientoEtiquetas=np.vstack((entrenamientoEtiquetas,etiquetas_onehot[posiciones]))
		#ventanas=np.delete(ventanas,posiciones,0) #eliminamos etiquetas y ventanas de el conjunto original
		#etiquetas=np.delete(ventanas,posiciones,0)
		etiquetas[posiciones]=0		

validacion=None
validacionEtiquetas=None
for fono in fonos:
	posiciones=np.argwhere(etiquetas==fono)
	np.random.shuffle(posiciones) #mezclamos el arreglo para obtener posiciones aleatorias ,ya que el listado es de elementos contiguos
	tam=posiciones.size 
	posiciones=posiciones[0:tam]#recortamos el arreglo de posiciones unicamente al tamaño que usaremos
	if(validacion==None):
		validacion=ventanas[posiciones]
		validacionEtiquetas=etiquetas_onehot[posiciones]
		#ventanas=np.delete(ventanas,posiciones,0) #eliminamos etiquetas y ventanas de el conjunto original
		#etiquetas=np.delete(ventanas,posiciones,0)
		#etiquetas[posiciones]=0
	else:
		validacion=np.vstack((validacion,ventanas[posiciones]))
		validacionEtiquetas=np.vstack((validacionEtiquetas,etiquetas_onehot[posiciones]))
		#ventanas=np.delete(ventanas,posiciones,0) #eliminamos etiquetas y ventanas de el conjunto original
		#etiquetas=np.delete(ventanas,posiciones,0)
		#etiquetas[posiciones]=0

prueba=np.reshape(prueba,(prueba.shape[0],-1))
pruebaEtiquetas=np.reshape(pruebaEtiquetas,(pruebaEtiquetas.shape[0],-1))
np.save('prueba.npy',prueba)
np.save('pruebaEtiquetas.npy',pruebaEtiquetas)

entrenamiento=np.reshape(entrenamiento,(entrenamiento.shape[0],-1))
entrenamientoEtiquetas=np.reshape(entrenamientoEtiquetas,(entrenamientoEtiquetas.shape[0],-1))
np.save('entrenamiento.npy',entrenamiento)
np.save('entrenamientoEtiquetas.npy',entrenamientoEtiquetas)

validacion=np.reshape(validacion,(validacion.shape[0],-1))
validacionEtiquetas=np.reshape(validacionEtiquetas,(validacionEtiquetas.shape[0],-1))
np.save('validacion.npy',validacion)
np.save('validacionEtiquetas.npy',validacionEtiquetas)


#ventanas,etiquetas=shuffle(ventanas,etiquetas) #mezclamos los arreglos de manera igual en am

#cantEntrenamiento=int(ventanas.shape[0]*.75)
#cantPrueba=ventanas.shape[0]-cantEntrenamiento

#np.save('entrenamiento.npy',ventanas[:cantEntrenamiento,])
#serie=pd.Series(etiquetas[:cantEntrenamiento,])
#one_hot=pd.get_dummies(serie).get_values()
#np.save('etiquetasEntrenamiento.npy',one_hot)

#np.save('prueba.npy',ventanas[cantEntrenamiento:,])
#serie=pd.Series(etiquetas[cantEntrenamiento:,])
#one_hot=pd.get_dummies(serie).get_values()

#np.save('etiquetasPrueba.npy',one_hot)