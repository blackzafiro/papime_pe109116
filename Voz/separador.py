#http://pydub.com/
#3 enero agregado separador por milisegundos
from pydub import AudioSegment
import sys

def guardar(segmento,nombreArchivo,etiqueta):
    print("Creando: "+nombreArchivo+"-"+etiqueta)
    segmento.export(nombreArchivo+"-"+etiqueta,format="wav")
    
def separar(audio,nombreArchivo,etiquetas,tmin=0):
    '''Genera archivos de audio con la duracion estipulada por las etiquetas,con el argumento tmin garantiza que los 
    archivos tengan al menos tmin ms de duracion recorriendo el final del audio el tiempo necesario para igualar la duracion requerida'''
    for etiqueta in etiquetas:
        aux=etiqueta.split()
        aux[0]=float(aux[0].replace(',','.'))*1000
        aux[1]=float(aux[1].replace(',','.'))*1000
        print(aux[0] , aux[1])
        if(tmin>0):
            duracion=aux[1]-aux[0]
            if(duracion<tmin):
                duracion+=tmin-duracion
                aux[1]+=duracion
                aux[1]=int(aux[1])

        tmp=audio[aux[0]:aux[1]]
        guardar(tmp,nombreArchivo,aux[2])
        
def separarAudio(nombreArchivo,nombreEtiquetas,tmin=0): 
	try:       
		audio=AudioSegment.from_wav(nombreArchivo)
		etiquetas=open(nombreEtiquetas,'r').readlines()
		separar(audio,nombreArchivo,etiquetas,tmin)
		
	except IOError:
		print('Alguno de los archivos '+nombreArchivo+'.wav o de etiquetas no existe')

def separarAudioMs(nombreArchivo,ms):
    #separa un archivo creando ventanas de "ms" milisegundos
    try:
        audio=AudioSegment.from_wav(nombreArchivo)
        duracion=len(audio)

        rangos=[ [inicio,inicio+ms] for inicio in range(0,duracion) if (inicio+ms)<duracion]

        for r in rangos:
            guardar(audio[r[0]:r[1]],nombreArchivo,"->"+str(r[0]))
    except IOError:
        print('Algun problema en separarAudioMS')

if __name__=='__main__':
    nombreArchivo=sys.argv[1]
    nombreEtiquetas=sys.argv[2]
    #print( nombreArchivo,nombreEtiquetas)
    audio=AudioSegment.from_wav(nombreArchivo)
    etiquetas=open(nombreEtiquetas,'r').readlines()
    separar(audio,nombreArchivo,etiquetas)
    

