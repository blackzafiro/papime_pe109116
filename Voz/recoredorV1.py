#Version 1.1

#Benjamin Torres Saavedra
#20 octubre 2016
#7 de noviembre 2016 agregado separador de archivos de audio
#15 de noviembre 2016 agregado calculo de primera y segunda derivadas
#Utilerias para recorrer directorios y ejecutar tareas sobre los archivos contenidos

import os
import time
import subprocess
import numpy as np
import sys
from separadorV1 import separarAudioMs
import separadorV1 as separador
from colector import Colector
#import librosa
fonos=['a','am','ama','ari','illa','lla','mar','na','ona','rill','zo','zon','anj','ara','nar','ran','egr','o','gra','ne','neg','ab','aba','ado','ag','aga','al','alt','alu','anz','arra','az','azu','ban','be','ber',
'bu','bus','cha','da','dad','de','der','do','e','echa','ede','el','erd','ere','es','esp','etr','garr','ier','ira',
'iz','izk','ja','ji','jir','jo','ka','kie','la','lto','lud','nja','nza','ojo','onj','oze','pon','ra','rda','rde',
'rech','roz','rra','rre','rret','rro','rroj','sa','sal','sil','ska','spo','to','tro','uda','ul','usk','za','zed','zki','zul']

VENTANA=17  
CARACTERISTICAS=13
OCUPADAS=5
colector=None
def extraerCaracteristicas(raiz=os.getcwd(),config=os.getcwd(),ligero=False):#:)
	'''Ejecuta HCopy sobre todos los elementos en el arbol del directorio indicado como parametro,
	config corresponde con el archivo de configuracion de HTK'''
	
	hcopyParams=['HCopy','-C',config+'/config.plp','aqui nombre entrada','aqui nombre salida']
	etiquetas=[]#contendra todas las etiquetas
	total=[]#contendra las caracteristicas de todos los archivos
	for directorio, subdirList, listaArchivos in os.walk(raiz):
	    print('Directorio encontrado: %s' % directorio)
	    for archivo in listaArchivos:
			if('->' in archivo and not('.htk'in archivo) ): #procesa solo archivos que ya sean fragmentados en ms
				hcopyParams[3]=directorio+'/'+archivo
				hcopyParams	[4]=directorio+'/'+archivo+'-plp'
				subprocess.call(hcopyParams)#Ejecutamos HCopy para extraer caracteristicas
				aux=leer_archivo_caracteristicas(directorio+'/'+archivo+'-plp.txt').flatten()
				fono=archivo[archivo.find("-")+1:archivo.find("-->")]#obtenemos el fono que se encuentra en el archivo

				'''print("Procesando:"+directorio+'/'+archivo+'-plp.txt')'''
				if(total==[]):
					if(len(aux)==338):
						total=aux
					elif(len(aux)<338):
						total=aux+[0]*(338-len(aux))
					else:
						total=aux[:338]
					etiquetas=fono
				else:
					if(len(aux)==338):
						pass
					elif(len(aux)<338):
						aux=aux+[0]*(338-len(aux))
					else:
						aux=aux[:338]

					total=np.vstack((total,[aux]))
					etiquetas=np.vstack((etiquetas,fono))#print('a:',aux.shape)

	np.save(raiz+'/ventanas.npy',total)
	np.save(raiz+'/etiquetas.npy',etiquetas)

def extract_feature(file_name):
    #X, sample_rate = librosa.load(file_name)
    X=file_name
    sample_rate=44100
    stft = np.abs(librosa.stft(X))
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T,axis=0)
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T,axis=0)
    #mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T,axis=0)
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T,axis=0)
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T,axis=0)
    #return mfccs,chroma,mel,contrast,tonnetz
    char= np.hstack([stft,mfccs,chroma,contrast,tonnetz])
    return char

def normalizar(matriz):
	'''Normaliza los valores de una matriz proporcionada a valores entre 0 y 1'''
	max=np.amax(matriz,axis=0)
	min=np.amin(matriz,axis=0)
	matriz=(datos-min)/(max-min)

def leer_archivo_caracteristicas(archivo,normalizar=False):#:)
	'''Lee los archivos de HTK y los devuelve en forma matricial(#,13),si se pasa como argumento
	True,los datos tambien seran normalizados'''
	aux=np.fromfile(archivo,dtype=float,sep=" ")
	aux=np.reshape(aux,(-1,13))
	if(normalizar):
		aux=normalizar(aux)
	return aux

def crear_ventanas(matriz,tamanio):
	'''Devuelve una matriz nueva de 3 dimensiones,cuyos elementos en la primera dimension
	son agrupaciones de @tamanio renglones pasadas en la matriz original,en caso de que la matriz
	no contenga suficientes elementos para crear ventanas,se rellenara para crear al menos una'''
	#print("**"+str(len(matriz)))
	#print("**"+str(tamanio))
	tmp=np.copy(matriz)
	tmp=tmp[:,:OCUPADAS]
	print("tmp=",tmp.shape)
	if (len(tmp)<tamanio):#si no hay suficientes frames para crear una ventana se agregan ceros a ambos lados
		nuevas=tamanio-len(tmp)/2
		zerosaux=np.zeros((nuevas,OCUPADAS)) #renglones de ceros con |caracteristicas| elementos
		tmp=np.vstack((zerosaux,tmp,zerosaux))#rodeamos las caracteristicas con ceros

	primeraDerivada=calcularDerivada(tmp)
	segundaDerivada=calcularDerivada(primeraDerivada)
	tmp=np.hstack((tmp,primeraDerivada,segundaDerivada))

	aux=np.zeros((len(tmp)-(tamanio-1),tamanio,OCUPADAS*3))#son 13 caracteristicas,incluido C0
	for i in range(0,len(aux)):
		aux[i]=tmp[i:i+tamanio,:]
	#print("tmpf=",tmp.shape)
	return aux

def calcularDerivada(matriz):
	'''Calcula la derivada de una matriz de elementos,donde cada renglon es una muestra en el tiempo ,es decir,calcula tn - tn-1'''
	derivada=np.zeros(matriz.shape)
	for pos,elem in enumerate(matriz):
		if pos==0:#la primera posicion es identica,pues t[n-1] no existe
			derivada[0]=matriz[pos]
		else:
			derivada[pos]=matriz[pos]-matriz[pos-1]

	return derivada

def generarSegmentosAudio(dics="EVP",tmin=100):
	#Separa archivos por etiquetas
	'''Recorre el arbol de directorios buscando archivos .wav y separandolos de acuerdo a sus etiquetas en archivos del mismo prefijo y con 'fonos.txt'''
	dicts=[]
	global colector
	if len(dics)!=0:
		if('E' in dics): 
			dicts.append(colector.entrenamiento)
		if('V' in dics):
			dicts.append(colector.validacion)
		if('P' in dics):
			dicts.append(colector.prueba)
	for dic in dicts:
		for k in dic.keys():
			print("d:",dic[k])
			separador.separarAudio(dic[k][0],dic[k][2],tmin)#1 es la ubicacion .wav ,3 es de las etiquetas

def generarFonos(conjunto,tmin=100):#Despreciada
	#Separa los archivos en fonos y reune fonos similares en las carpetas correspondientes a cada uno
	#generarSegmentosAudio(dicts,tmin)#separa por fonos, generando archivos de duracion minima tmin ms
	pass

def dividirenms(dics="EVP",ms=100):
	#divide archivos .wav en una carpeta en fragmentos de "ms" ms,agrega etiqueta 
	dicts=[]
	if len(dics)!=0:
		if('E' in conjunto): 
			dicts.append(colector.entrenamiento)
		if('V' in conjunto):
			dicts.append(colector.validacion)
		if('P' in conjunto):
			dicts.append(colector.prueba)

	for dic in dicts:
		for k in dic.keys():
			for _,_,listaArchivos in os.walk(dic[k][0]+"Seg"):
				separarAudioMS(dic[k][0]+"Seg/"+listaArchivos)

	'''for directorio, subdirList, listaArchivos in os.walk(carpeta):	
	    print('Directorio encontrado: %s' % directorio)
	    
	    for archivo in listaArchivos:
			if('.wav' in archivo and ('-'in archivo) ):
				separarAudioMs(directorio+"/"+archivo,ms)'''
				
def recolectarVentanas(carpeta=os.getcwd()):
	#Recolecta todas las ventanas en el directorio "fonos"
	#print("###CARPETA:",carpeta,"###")
	subdirectorios=os.listdir(carpeta)
	
	ventana=None
	etiquetas=None
	for subd in subdirectorios:
		sub=carpeta+subd
		if not os.path.isdir(sub):
			next
		else:
			print('Directorio encontrado %s' %sub)
			if(ventana==None):
				#print(sub+'/ventanas.npy')
				ventana=np.load(sub+'/ventanas.npy') #cargamos la ventana de cada carpeta
				muestras=ventana.shape[0]
				etiquetas=np.array([subd]*muestras)
				print("!!!",etiquetas.shape)
			else:
				try:
					tmp=np.load(sub+'/ventanas.npy')
					muestras=tmp.shape[0]
					print("ventana=>",ventana.shape)
					print("tmp=>",tmp.shape)
					ventana=np.vstack((ventana,tmp))
					etiquetas=np.append(etiquetas,np.array([subd]*muestras) )
				except:
					pass

	ventana=np.reshape(ventana,(ventana.shape[0],-1))
	np.save(carpeta+"/ventanasTotales.npy",ventana)
	np.save(carpeta+"/etiquetasVentanasTotales.npy",etiquetas)

def realizarTodas(colecciones="EVP"):
	'''Genera segmentos con fonos,luego ventanas de milisegundos,extrae caracteristicas de las ventanas y recopila'''
	print("Separando por fonos")
	generarSegmentosAudio("EVP",100)
	print("Separando fonos por ms")
	dividirenms("EVP",100)
	dicts=[colector.entrenamiento,colector.validacion,colector.prueba]
	print("Extrayendo caracteristicas")
	for dic in dicts:
		for k in dic.keys():
			ruta=dic[k][0]+"Seg/"
			extraerCaracteristicas(ruta) #asumire config.plp en el directorio de ejecucion actual
			#recolectarVentanas(ruta)
			#for sub in os.listdir(ruta):
	print("Recolectando ventanas globales")
	recolectarVentanas()	

def resolverColecciones():
	print("Iniciando...")
	archivos=["entrenamiento.txt","validacion.txt","prueba.txt"]
	prefijos=["E","V","P"]
	global colector
	colector=Colector("vault",archivos,prefijos)
	

if __name__=='__main__':
	resolverColecciones()
	if len(sys.argv)<1:
		print('argumentos insuficientes')
	else:
		if(sys.argv[1]=='recolectar'):
			recolectarVentanas(sys.argv[2])
		if(sys.argv[1]=='audio'):
			generarSegmentosAudio()
		if(sys.argv[1]=='plp'):
			extraerCaracteristicas(sys.argv[2],sys.argv[3])#carpeta donde se realizara el recorrido
		if(sys.argv[1]=='fonos'):#realiza todas las tareas para generar ventanas
			generarSegmentosAudio("EVP",int(sys.argv[2]))
		if(sys.argv[1]=='total'):#realiza todas las tareas para generar ventanas
			realizarTodas("EVP")
		if(sys.argv[1]=='dividirenms'):#separa audios por ms
			dividirenms("EVP",int(sys.argv[3]))
		if(sys.argv[1]=='rescate'):
			rescatar(sys.argv[2])


