#-*-coding:utf8-*-
#Version 1.1
#http://pydub.com/
#3 enero agregado separador por milisegundos
from pydub import AudioSegment
import sys
import os
from os.path import basename
import numpy as np

def generarIdeales(nombreArchivoEtiquetas,ms=100,tolerancia=500):
    '''Genera la secuencia de etiquetas ideales de un archivo
    --------
    ms es el tamaño aproximado de las ventanas
    tolerancia(en ms) entre etiquetas para agregar relleno 
    '''
    secuencia=[]
    etiquetas=open(nombreArchivoEtiquetas,'r').readlines()
    #t_anterior=0#tiempo final de el ultimo fono colocado
    #fono_anterior="sil"
    for i,etiqueta in enumerate(etiquetas):
            
        et=etiqueta.split()
        inicio=float(et[0].replace(',','.'))*1000
        final=float(et[1].replace(',','.'))*1000
        tag=et[2]
        secuencia.append((inicio,final,tag))

#        if(inicio-t_anterior>tolerancia):#si hay espacios de mas de "toletancia"ms repetimos la ultima etiqueta usada
#            for i in range(int(t_anterior),int(inicio)+ms):
#                secuencia.append(fono_anterior)
#                
#        for i in range(int(inicio),int(final)+ms):
#            secuencia.append(tag)
#
#        fono_anterior=tag
#        t_anterior=int(final)
#
    duracionTotal=int(secuencia[-1][1])#obtenemos la segunda componente del ultimo elemento 
    rangos=[ [inicio,inicio+ms] for inicio in range(0,duracionTotal,100) if (inicio+ms)<duracionTotal]


    listaEtiquetas=["sil"]*len(rangos)
    for tag in secuencia:
        t1=(tag[0]//100)*100
        t2=(tag[1]//100)*100
        for i,tiempos in enumerate(rangos):
            if(t1>=tiempos[0] and t1<=tiempos[1]):
                if(listaEtiquetas[i]=="sil"):    
                    listaEtiquetas[i]=tag[2]
            
            elif(t2>=tiempos[0] and t2<=tiempos[1]):
                if(listaEtiquetas[i]=="sil"):    
                    listaEtiquetas[i]=tag[2]

    
    destino=nombreArchivoEtiquetas[:nombreArchivoEtiquetas.find(".txt")]
    #ideales=np.array(secuencia)
    ideales=np.array(listaEtiquetas)
    np.save(destino+"Ideal.npy",ideales)
    return ideales

def guardar(segmento,carpeta,nombreArchivo,etiqueta):
    '''Guarda un segmento dentro de "carpeta" con el nombre + etiqueta '''
    rutaFinal=carpeta+"/"+nombreArchivo+"-"+etiqueta
    #print("Creando: "+rutaFinal)
    if(not os.path.exists(carpeta)):
        os.makedirs(carpeta)
    segmento.export(rutaFinal,format="wav")
    #print("CREADO")
    
def separar(audio,carpeta,nombreArchivo,etiquetas,tmin=0):
    '''Genera archivos de audio con la duracion estipulada por las etiquetas,con el argumento tmin garantiza que los 
    archivos tengan al menos tmin ms de duracion recorriendo el final del audio el tiempo necesario para igualar la duracion requerida
    los archivos se guardaran en "carpeta" '''
    for etiqueta in etiquetas:
        aux=etiqueta.split()
        aux[0]=float(aux[0].replace(',','.'))*1000
        aux[1]=float(aux[1].replace(',','.'))*1000
        #print("Duracion etiqueta:",aux[0] , aux[1],"ms")
        if(tmin>0):
            duracion=aux[1]-aux[0]
            if(duracion<tmin):
                duracion+=tmin-duracion
                aux[1]+=duracion
                aux[1]=int(aux[1])

        tmp=audio[aux[0]:aux[1]]
        guardar(tmp,carpeta,nombreArchivo,aux[2])
        
def separarAudio(carpeta,nombreArchivo,nombreEtiquetas,tmin=0): 
    '''Carpeta= carpeta donde se buscara el archivo'''
    try:       
        #print("segmento:",carpeta+"/"+nombreArchivo)
        audio=AudioSegment.from_wav(carpeta+"/"+nombreArchivo)
        #print("et:",carpeta+"/"+nombreEtiquetas)
        etiquetas=open(carpeta+"/"+nombreEtiquetas,'r').readlines()
        carpetaDeSegmentos=carpeta+"/"+nombreArchivo+"Seg"
        separar(audio,carpetaDeSegmentos,nombreArchivo,etiquetas,tmin)
            
    except IOError:
        print('Alguno de los archivos '+nombreArchivo+' o de etiquetas no existe')

def separarAudioMs(carpeta,nombreArchivo,ms):
    #separa un archivo creando ventanas de "ms" milisegundos
    try:
        #print(carpeta+"/"+nombreArchivo)
        audio=AudioSegment.from_wav(carpeta+"/"+nombreArchivo)
        duracion=len(audio)

        rangos=[ [inicio,inicio+ms] for inicio in range(0,duracion) if (inicio+ms)<duracion]

        for r in rangos:
            guardar(audio[r[0]:r[1]],carpeta,nombreArchivo,"->"+str(r[0]))
    except IOError:
        print('Algun problema en separarAudioMS')

if __name__=='__main__':
    nombreArchivo=sys.argv[1]
    ideales=generarIdeales(nombreArchivo)
    

    #nombreEtiquetas=sys.argv[2]
    #print( nombreArchivo,nombreEtiquetas)
    #audio=AudioSegment.from_wav(nombreArchivo)
    #etiquetas=open(nombreEtiquetas,'r').readlines()
    #separar(audio,nombreArchivo,etiquetas)
    

