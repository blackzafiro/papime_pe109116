import os
import sys
import time
import subprocess
import numpy as np
from pydub import AudioSegment
import shutil
from separadorV1 import separarAudioMs
import separadorV1 as separador
from colector import Colector
from recoredorV1 import leer_archivo_caracteristicas

#NO SE POR QUE ESTA EL PARAMETRO "LIMITE"
def extraerCaracteristicas(nombreArchivo,limite,raiz=os.getcwd(),config=os.getcwd()):#:)
	'''Ejecuta HCopy sobre todos los elementos en el arbol del directorio indicado como parametro en
	orden numerico,
	nombreArchivo es el nombre del archivo con el resultado
	raiz es la carpeta donde se realizara el recorrido
	config corresponde con el archivo de configuracion de HTK
	'''
	print("Calculando de:"+raiz)
	hcopyParams=['HCopy','-C',"config",'aqui nombre entrada','aqui nombre salida']
	total=[]#contendra las caracteristicas de todos los archivos
	correcciones=0
	#print("RaizHCopy:",raiz)
	for directorio, subdirList, listaArchivos in os.walk(raiz):

		for archivo in sorted(listaArchivos,key=int): #LEEMOS LOS ARCHIVOS EN ORDEN NUMERICO
			if(not('plp' in archivo)): 
				hcopyParams[3]=directorio+"/"+archivo
				hcopyParams	[4]=directorio+"/"+archivo+'-plp'
				subprocess.call(hcopyParams)#Ejecutamos HCopy para extraer caracteristicas
				aux=leer_archivo_caracteristicas(directorio+"/"+archivo+'-plp.txt').flatten()

				if(total==[]):
					if(len(aux)==338):
						total=aux
					elif(len(aux)<338):
						total=np.hstack( (aux,[0]*(338-len(aux)) )  ) 
						correcciones+=1
					else:
						total=aux[:338]
					
				else:
					if(len(aux)==338):
						pass
					elif(len(aux)<338):
						aux=np.hstack( (aux,[0]*(338-len(aux)) )  ) 
						correcciones+=1
					elif(len(aux)>338):
						aux=aux[:338]
				
				total=np.vstack((total,aux[:338]))
			else:
				print("archivo no util")
				next

	#print("Correcciones:",correcciones)
	print(raiz+nombreArchivo+".npy")
	np.save(raiz+nombreArchivo+".npy",total)
	
def extractor(carpeta,archivo,ms=100):
	'''Se encarga de extraer las caracteristicas PLP de un archivo ubicado en carpeta+archivo de fragmentos de ms milisegundos
	y generar un archivo con las ventanas en orden secuencial
	'''
	try:
		nombreArchivo=carpeta+archivo
		tmp=carpeta+"tmp"
		audio=AudioSegment.from_wav(nombreArchivo)
		
		if(os.path.exists(tmp)):
			print("Eliminando tmp")
			shutil.rmtree(tmp)

		os.mkdir(tmp)
		
		duracion=len(audio)
		rangos=[ [inicio,inicio+ms] for inicio in range(0,duracion,100) if (inicio+ms)<duracion]
		
		for i,r in enumerate(rangos):
			#print(carpeta+"/tmp/"+str(1))
			audio[r[0]:r[1]].export(carpeta+"tmp/"+str(i),format="wav")
		
		extraerCaracteristicas(nombreArchivo=archivo,limite=len(rangos),raiz=tmp)	

	except IOError, e:
		print 'No such file or directory: %s' % e
		print('Algun problema en extractor')

if __name__ == '__main__':
	carpeta=sys.argv[1]
	print(carpeta)
	for directorio, subdirList, listaArchivos in os.walk(carpeta):
	    for archivo in listaArchivos:
	    	if not('npy' in archivo):
				print(archivo)
				extractor(carpeta,archivo)
