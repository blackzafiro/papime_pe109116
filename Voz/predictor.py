import pickle
import os
from keras.models import Model,load_model
import numpy as np
#from hmmlearn import hmm
from utilidades_hmm import dic_fonos
from seqlearn.hmm import MultinomialHMM
traduccion=pickle.load(open("one-hotAfonos.pkl","rb"))

carpetaArchivos="simpleEntrenamiento/"
carpetaEtiquetas="../tags/"
archivosEtiquetas=["agarra41PfonosIdeal.npy","dadov5fonosIdeal.npy","veam4PfonosIdeal.npy",
"agarra41fonosIdeal.npy","esponja1fonosIdeal.npy","veam5fonosIdeal.npy",
"agarra43PfonosIdeal.npy","esponja4PfonosIdeal.npy","vedada3fonosIdeal.npy",
"agarra43fonosIdeal.npy","esponja5fonosIdeal.npy","vedada4PfonosIdeal.npy",
"agarra53fonosIdeal.npy","gder2fonosIdeal.npy","vedada4fonosIdeal.npy",
"agarra61fonosIdeal.npy","gder4PfonosIdeal.npy","vedada6fonosIdeal.npy",
"agarra62fonosIdeal.npy","gder4fonosIdeal.npy","vedadr1fonosIdeal.npy",
"agarra64fonosIdeal.npy","gder6fonosIdeal.npy","vedadr3fonosIdeal.npy",
"alto1fonosIdeal.npy","gizq1fonosIdeal.npy","vedadr5fonosIdeal.npy",
"alto2fonosIdeal.npy","gizq4PfonosIdeal.npy","vedav1fonosIdeal.npy",
"alto5fonosIdeal.npy","gizq5fonosIdeal.npy","vedav2fonosIdeal.npy",
"avanza1fonosIdeal.npy","retrocede1fonosIdeal.npy","veesp2fonosIdeal.npy",
"avanza3fonosIdeal.npy","retrocede2fonosIdeal.npy","veesp3fonosIdeal.npy",
"avanza6fonosIdeal.npy","retrocede6fonosIdeal.npy","vena3fonosIdeal.npy",
"dadoa4fonosIdeal.npy","saluda2fonosIdeal.npy","vena4fonosIdeal.npy",
"dadoa5fonosIdeal.npy","saluda3fonosIdeal.npy","veneg2fonosIdeal.npy",
"dador4fonosIdeal.npy","saluda5fonosIdeal.npy","veneg3fonosIdeal.npy",
"dador6fonosIdeal.npy","veam1fonosIdeal.npy","veneg4fonosIdeal.npy",
"dadov4fonosIdeal.npy","veam2fonosIdeal.npy","veneg6fonosIdeal.npy"]

archivos=["tmpagarra41P.wav.npy","tmpdadov5.wav.npy","tmpveam4P.wav.npy",
"tmpagarra41.wav.npy","tmpesponja1.wav.npy","tmpveam5.wav.npy",
"tmpagarra43P.wav.npy","tmpesponja4P.wav.npy","tmpvedada3.wav.npy",
"tmpagarra43.wav.npy","tmpesponja5.wav.npy","tmpvedada4P.wav.npy",
"tmpagarra53.wav.npy","tmpgder2.wav.npy","tmpvedada4.wav.npy",
"tmpagarra61.wav.npy","tmpgder4P.wav.npy","tmpvedada6.wav.npy",
"tmpagarra62.wav.npy","tmpgder4.wav.npy","tmpvedadr1.wav.npy",
"tmpagarra64.wav.npy","tmpgder6.wav.npy","tmpvedadr3.wav.npy",
"tmpalto1.wav.npy","tmpgizq1.wav.npy","tmpvedadr5.wav.npy",
"tmpalto2.wav.npy","tmpgizq4P.wav.npy","tmpvedav1.wav.npy",
"tmpalto5.wav.npy","tmpgizq5.wav.npy","tmpvedav2.wav.npy",
"tmpavanza1.wav.npy","tmpretrocede1.wav.npy","tmpveesp2.wav.npy",
"tmpavanza3.wav.npy","tmpretrocede2.wav.npy","tmpveesp3.wav.npy",
"tmpavanza6.wav.npy","tmpretrocede6.wav.npy","tmpvena3.wav.npy",
"tmpdadoa4.wav.npy","tmpsaluda2.wav.npy","tmpvena4.wav.npy",
"tmpdadoa5.wav.npy","tmpsaluda3.wav.npy","tmpveneg2.wav.npy",
"tmpdador4.wav.npy","tmpsaluda5.wav.npy","tmpveneg3.wav.npy",
"tmpdador6.wav.npy","tmpveam1.wav.npy","tmpveneg4.wav.npy",
"tmpdadov4.wav.npy","tmpveam2.wav.npy","tmpveneg6.wav.npy"]


def procesar_archivo(modelo,nombre):
	archivo=np.load(carpetaArchivos+nombre)
	return modelo.predict(archivo)

def one_hot(muestras):
	nueva=np.zeros(muestras.shape)
	m=np.argmax(muestras,axis=1)

	for i,renglon in enumerate(nueva):
		renglon[m[i]]=1	
	return nueva

def traducir(muestras):
	m=one_hot(muestras) #elegimos el fono mas probable

	resultado=[]
	resultado_numerico=[]
	for i,o in enumerate(m):
		buscable=tuple(o.tolist())#convertimos el one-hot a una tupla(que es hasheable)
		fono=traduccion[buscable] #buscamos la etiqueta en string de el one-hot
		fono_numerico=dic_fonos[fono] #buscamos la etiqueta numerica de el one-hot
		resultado.append(fono)
		resultado_numerico.append(fono_numerico)

	return np.array(resultado),np.array(resultado_numerico)

#print "Introduce nombre del modelo a cargar"
#kerasModel-20-0.59.hdf5 
#autoencoder=load_model(raw_input())
autoencoder=load_model("kerasModel-20-0.59.hdf5")

#secuencias=pickle.load('secuenciasIdeales.pkl')

entrenamiento=None
etiquetas=[]
longitudes=[]
for i,archivo in enumerate(archivos):
	print (i,"/",len(archivos)-1)
	if(i==0):
		res=procesar_archivo(autoencoder,archivo)
		res=one_hot(res)
		#print(type(res_n))
		entrenamiento=res
		et=np.load(carpetaEtiquetas+archivosEtiquetas[i])
		if(len(et)>len(res)):
			et=et[:len(res)]
		elif(len(et)<len(res)):
			diferencia=len(res)-len(et)
			et=np.concatenate([et,['sil']*diferencia])

		etiquetas=et
		longitudes.append(len(et))

	else:
		res=procesar_archivo(autoencoder,archivo)
		res=one_hot(res)
		#print(type(res_n))		
		entrenamiento=np.concatenate([entrenamiento,res])
		et=np.load(carpetaEtiquetas+archivosEtiquetas[i])
		if(len(et)>len(res)):
			et=et[:len(res)]
		elif(len(et)<len(res)):
			diferencia=len(res)-len(et)
			et=np.concatenate([et,['sil']*diferencia])

		etiquetas=np.concatenate([etiquetas,et])
		longitudes.append(len(res))

HMM=MultinomialHMM()
HMM.fit(entrenamiento,etiquetas,longitudes)	



'''if os.path.exists('REVISAR.pkl'):
	HMM=pickle.load(open('REVISAR.pkl','rb'))
else:
	HMM = hmm.GaussianHMM(n_components=72, covariance_type="diag",n_iter=100)
	HMM.fit(entrenamiento,longitudes)
	pickle.dump(HMM,open('REVISAR.pkl','wb'))


carpeta="simpleValidacion/"
prueba=one_hot(procesar_archivo(autoencoder,"tmpalto3.wav.npy"))

'''