# README #

Soluciones a las prácticas del Laboratorio de aprendizaje de máquina multimodal.

### Participantes ###

* Benjamín Torres Saavedra
  Reconocimiento de voz
* José Antonio Vilchis Salazar
  Aprendizaje reforzado
* Vladimir Mijail Aaron Gutiérrez Valdez
  Reconocimiento visual de objetos
