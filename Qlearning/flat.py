
import numpy as np
np.set_printoptions(threshold=np.nan)
import cv2

gamma = 0.5
reward = 0.1

def leer():
    n = 1
    
    while(True):     
        ext = ".txt"
        name = 'output' + str(n)
        lista = []
        try:
            cap = cv2.VideoCapture(name + '.avi')
            if not cap.isOpened():
                print("No has mas archivos " + str(n))
                break
            f = open(name + ext,'w')
            ret, frame = cap.read()
            while(True):
                
                if not ret:
                    #error
                    break
                array = np.array(frame,copy = True)
                array = np.asfarray(array,dtype='int32')

                #Hacer flat de hasta 4 columnas antes de terminar
                flat = array[:,:-4,:]
                flat = flat.flatten()
                #flat = flat / 255.0
                
                #vector de 5 one-hot para el estado
                estado = array[0,-4,:].flatten()
                if(estado[0] < 128):
                    estado[0] = 0
                else:
                    estado[0] = 255
                if(estado[1] < 128):
                    estado[1] = 0
                else:
                    estado[1] = 255
                if(estado[2] < 128):
                    estado[2] = 0
                else:
                    estado[2] = 255
                #atras
                if estado[0] == 255 and estado[1] == 255 and estado[2] ==255:
                    estado = np.array([0,255,0,0,0])
                #izquierda
                elif estado[0] == 255:
                    estado = np.array([0,0,255,0,0])
                #adelante
                elif estado[1] == 255:
                    estado = np.array([255,0,0,0,0])
                #derecha
                elif estado[2] == 255:
                    estado = np.array([0,0,0,255,0])
                #quieto
                else:
                    estado = np.array([0,0,0,0,255])

                #vector de 5 one hot para la orden
                orden = array[0,-2,:].flatten()
                if(orden[0] < 128):
                    orden[0] = 0
                else:
                    orden[0] = 255
                if(orden[1] < 128):
                    orden[1] = 0
                else:
                    orden[1] = 255
                if(orden[2] < 128):
                    orden[2] = 0
                else:
                    orden[2] = 255
                #atras
                if orden[0] == 255 and orden[1] == 255 and orden[2] ==255:
                    orden = np.array([0,255,0,0,0])
                #izquierda
                elif orden[0] == 255:
                    orden = np.array([0,0,255,0,0])
                #adelante
                elif orden[1] == 255:
                    orden = np.array([255,0,0,0,0])
                #derecha
                elif orden[2] == 255:
                    orden = np.array([0,0,0,255,0])
                #quieto
                else:
                    orden = np.array([0,0,0,0,255])
                #vector de 1 para el bumper
                if(estado[1] == 255):
                    bumper = np.array([255])
                else:
                    bumper = np.array([0])
                #vector de 1 para el premio
                premio = np.array([0])
                #escribir arreglo
                
                #volver a leer otro cuadro
                ret, frame = cap.read()
                
                #para revisar si ya es el el ultimo cuadro
                if not ret:
                    premio[0] = 255
                flat = np.hstack((flat,estado,bumper,orden,premio))  
                    
                #meter el arreglo al archivo
                
                #np.savetxt(f, flat[None,:],delimiter='\n',newline='|',fmt='%i')
                lista.append(flat)              
                #break
            lista = np.array(lista)
            r = np.array(lista.shape)
            r[0] = r[0] - 2
            r[1] = r[1] - 1
            while(r[0]>=0):
                lista[r[0],r[1]] = lista[r[0],r[1]] + gamma*lista[r[0]+1,r[1]] - reward*gamma*lista[r[0],r[1]-6]
                r[0] = r[0] - 1
            print("Iniciando escritura de archivo " + str(n))
            np.savetxt(f, lista, delimiter=' ',newline='\n',fmt='%i')
            print("Finalizo escritura de archivo " + str(n))
            f.close()
        except ValueError:
            print("error?")
            break
        print(n)
        f.close()
        n = n + 1
        #break
if __name__ == "__main__":
    leer()