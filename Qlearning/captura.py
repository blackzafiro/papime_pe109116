#!/usr/bin/env python

import rospy
import numpy as np
import cv2
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3
from kobuki_msgs.msg import SensorState

b = 0
cX = 848
cY = 480
X = 84
Y = 48
alpha = 0.1
betha = 0.5   
nX = (cX / 4)
nY = (cY / 4)
estado = np.zeros((Y,1,3),dtype="uint8")
extra = np.zeros((Y,1,3),dtype="uint8")
orden = np.zeros((Y,1,3),dtype="uint8")
premio = np.zeros((Y,1,3),dtype="uint8")
pub = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size = 10)
rospy.init_node('visionCaptura', anonymous=True) 

def cambio(data):
    global b
    
    if data.left_pwm == 0 and data.right_pwm == 0:
        b = 0
        estado[:,:,0] = 0
        estado[:,:,1] = 0
        estado[:,:,2] = 0
        
    else:
        if b != 1:
            estado[:,:,0] = orden[:,:,0]
            estado[:,:,1] = orden[:,:,1]
            estado[:,:,2] = orden[:,:,2]
    if data.bumper == 0:
        extra[:,:,0] = 0
        extra[:,:,1] = 0
        extra[:,:,2] = 0
    else:
        b = 1
        extra[:,:,0] = 255
        extra[:,:,1] = 255
        extra[:,:,2] = 255
        estado[:,:,0] = 255
        estado[:,:,1] = 255
        estado[:,:,2] = 255
        premio[:,:,0] = 127
        premio[:,:,1] = 127
        premio[:,:,2] = 127
        print("vv")
        pub.publish(Twist(Vector3(-alpha,0,0),Vector3(0,0,0)))

def test_opencv():
    
    rospy.Subscriber("/mobile_base/sensors/core",SensorState,cambio)
    
    ffile = open('file.txt','r')
    st = ffile.readline()
    ffile.close()
    name = 'output' + st
    inpt = int(st)
    inpt = inpt + 1
    ffile = open('file.txt','w+')
    ffile.write(str(inpt))
    ffile.close()
    #cap = cv2.VideoCapture('output.avi')
    cap = cv2.VideoCapture(0)
    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'MJPG')
    
    out = cv2.VideoWriter(name + '.avi',fourcc, 20.0, (2*X+4,Y))

    while(True):
        premio[:,:,0] = 0
        premio[:,:,1] = 0
        premio[:,:,2] = 0
        ret, frame = cap.read()
        if not ret:
            break
        key = cv2.waitKey(10)
        if key == ord('q'): 
            break
        if key == ord('a'): 
            print("<")
            pub.publish(Twist(Vector3(0,0,0),Vector3(0,0,betha)))
            orden[:,:,0] = 255
            orden[:,:,1] = 0
            orden[:,:,2] = 0 

        if key == ord('s'): 
            print("v")
            pub.publish(Twist(Vector3(-alpha,0,0),Vector3(0,0,0)))
            orden[:,:,0] = 255
            orden[:,:,1] = 255
            orden[:,:,2] = 255

        if key == ord('d'): 
            print(">")
            pub.publish(Twist(Vector3(0,0,0),Vector3(0,0,-betha)))
            orden[:,:,0] = 0
            orden[:,:,1] = 0
            orden[:,:,2] = 255 
 
        if key == ord('w'): 
            print("^")
            pub.publish(Twist(Vector3(alpha,0,0),Vector3(0,0,0)))
            orden[:,:,0] = 0
            orden[:,:,1] = 255
            orden[:,:,2] = 0  
        
        if key == ord('r'): 
            print("p")
            premio[:,:,0] = 255
            premio[:,:,1] = 255
            premio[:,:,2] = 255 
            break;     
        
        cachito = frame[nY:nY * 3,nX:nX * 3,:]
        frame = cv2.resize(frame,(X,Y))
        cachito = cv2.resize(cachito,(X,Y))
        frame = np.hstack((frame,cachito,estado,extra,orden,premio))

        cv2.imshow('frame', frame)
        out.write(frame)

    cap.release()
    out.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    test_opencv()
