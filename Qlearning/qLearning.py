
import tensorflow as tf
from qLearningData import QLearningData
from data import Data
import numpy as np
#np.set_printoptions(threshold=np.nan)
np.set_printoptions(precision=3)


class QLearning:

    def weight_variable(self, shape):
        initial = tf.truncated_normal(shape, stddev=0.1,dtype=tf.float32)
        return tf.Variable(initial)

    def conv2d(self, x, W,stride):
        return tf.nn.conv2d(x, W, strides=[1, stride, stride, 1], padding='VALID')
        
    def getIndex(self,X):
        index = 0
        value = 0
        r = np.array(X.shape[1])
        for i in xrange(r):
            if X[0,i] > value:
                index = i
                value = X[0,i]
        
        if value != 0:
            return index
        else:
            raise ValueError(X)
        
    def reshape(self,X):
        shape = X.shape[0]
        return np.reshape(X,(-1,shape))
        
    def getFeedDict(self,X1,X2,Z,Y,keep_prob,index):
        aux = np.array([Y[0,index]])
        if index == 1 :
            y_conv = self.y1_conv
            accuracy = self.accuracy1
            train_step = self.train_step1
            y_= self.y1_
        elif index == 2 :
            y_conv = self.y2_conv
            accuracy = self.accuracy2
            train_step = self.train_step2
            y_= self.y2_
        elif index == 3 :
            y_conv = self.y3_conv
            accuracy = self.accuracy3
            train_step = self.train_step3
            y_= self.y3_
        elif index == 4 :
            y_conv = self.y4_conv
            accuracy = self.accuracy4
            train_step = self.train_step4
            y_= self.y4_
        elif index == 5 :
            y_conv = self.y5_conv
            accuracy = self.accuracy5
            train_step = self.train_step5
            y_= self.y5_
        elif index == 6 :
            y_conv = self.y6_conv
            accuracy = self.accuracy6
            train_step = self.train_step6
            y_= self.y6_
        else:
            raise ValueError(index)
            
        aux = self.reshape(aux)
        feed_dict={self.x1:X1, self.x2:X2, self.z:Z, y_:aux, self.keep_prob: keep_prob}
        return y_conv, y_, accuracy, train_step, feed_dict
    
    def clasification(self,data_set):
        try:
            X1 = self.reshape(data_set.X1)
            X2 = self.reshape(data_set.X2)
            Z = self.reshape(data_set.Z)
            Y = self.reshape(data_set.Y)
            index = self.getIndex(Y)
            y_conv, y_, accuracy, train_step, feed_dict = self.getFeedDict(X1,X2,Z,Y,1.0,index)
            cls = np.empty([6])
            cls[0] = self.sess.run(self.y1_conv, feed_dict)
            cls[1] = self.sess.run(self.y2_conv, feed_dict)
            cls[2] = self.sess.run(self.y3_conv, feed_dict)
            cls[3] = self.sess.run(self.y4_conv, feed_dict)
            cls[4] = self.sess.run(self.y5_conv, feed_dict)
            cls[5] = self.sess.run(self.y6_conv, feed_dict)
            cls = self.reshape(cls)
            r = self.getIndex(cls)
            return r
        except ValueError as e:
            print 'Entrada invalida: ', e.value
            return -1
        except:
            print("Error en ejecucion para el ejemplar clasificado.")
            return -1
            
    def save(self):
        save_path = self.saver.save(self.sess, self.file_name)
        print("Modelo guardado en archivo: %s" % save_path)
    
    def train(self,data):
        X1 = data.X1
        X2 = data.X2
        Z = data.Z
        Y = data.Y
        size = X1.shape[0]
        try:
            for i in range(size):
                x1 = self.reshape(X1[i])
                x2 = self.reshape(X2[i])
                z = self.reshape(Z[i])
                y = self.reshape(Y[i])
                index = self.getIndex(y)
                y_conv, y_, accuracy, train_step, feed_dict = self.getFeedDict(x1,x2,z,y,0.5,index)
                train_step.run(feed_dict)
        except ValueError as e:
            print 'Entrada invalida: ', e.value
        
    def training(self):
        aux = self.QData.training_limit - self.QData.training_init
        for i in range(aux):
            try:
                data = self.QData.getNextTrainingData()
                self.train(data)
            except:
                print("Error en entrenamiento, ejemplar: " + str(self.QData.training))
        self.save()

    def test(self,data):
        X1 = data.X1
        X2 = data.X2
        Z = data.Z
        Y = data.Y
        size = X1.shape[0]
        rect = 0
        try:
            for i in range(size):
                x1 = self.reshape(X1[i])
                x2 = self.reshape(X2[i])
                z = self.reshape(Z[i])
                y = self.reshape(Y[i])
                index = self.getIndex(y)
                y_conv, y_, accuracy, train_step, feed_dict = self.getFeedDict(x1,x2,z,y,1,index)
                acc = accuracy.eval(feed_dict)
                rect = rect + acc
            rect = rect/size
            return rect
        except ValueError as e:
            print 'Entrada invalida: ', e.value
            
    def validating(self):
        aux = self.QData.validation_limit - self.QData.validation_init
        validation_prom = 0
        for i in range(aux):
            data = self.QData.getNextValidationData()
            validation_prom = validation_prom + self.test(data)
        validation_prom = validation_prom/aux
        return validation_prom
        
    def testing(self):    
        aux = self.QData.test_limit - self.QData.test_init
        test_prom = 0
        for i in range(aux):
            data = self.QData.getNextTestData()
            test_prom = test_prom + self.test(data)
        test_prom = test_prom/aux
        return test_prom
        
    def __init__(self):
        self.file_name = "/home/esmeralda/Documents/Qlearning/kobuki.ckpt"
        self.stride1 = 4
        self.stride2 = 2
        self.stride3 = 1
        
        self.sess = tf.InteractiveSession()
        
        self.y1_ = tf.placeholder(tf.float32, shape=[None, 1])
        self.y2_ = tf.placeholder(tf.float32, shape=[None, 1])
        self.y3_ = tf.placeholder(tf.float32, shape=[None, 1])
        self.y4_ = tf.placeholder(tf.float32, shape=[None, 1])
        self.y5_ = tf.placeholder(tf.float32, shape=[None, 1])
        self.y6_ = tf.placeholder(tf.float32, shape=[None, 1])
        
        self.z = tf.placeholder(tf.float32, shape=[None, 7])

        self.x1 = tf.placeholder(tf.float32, shape=[None, 12096])
        self.x1_image = tf.reshape(self.x1, [-1,48,84,3])

        self.W1_conv1 = self.weight_variable([4, 8, 3, 32])
        self.h1_conv1 = tf.nn.relu(self.conv2d(self.x1_image, self.W1_conv1, self.stride1))
        
        self.W1_conv2 = self.weight_variable([2, 4, 32, 64])
        self.h1_conv2 = tf.nn.relu(self.conv2d(self.h1_conv1, self.W1_conv2, self.stride2))
        
        self.W1_conv3 = self.weight_variable([2, 3, 64, 64])
        self.h1_conv3 = tf.nn.relu(self.conv2d(self.h1_conv2, self.W1_conv3, self.stride3))

        self.x2 = tf.placeholder(tf.float32, shape=[None, 12096])
        self.x2_image = tf.reshape(self.x2, [-1,48,84,3])

        self.W2_conv1 = self.weight_variable([4, 8, 3, 32])
        self.h2_conv1 = tf.nn.relu(self.conv2d(self.x2_image, self.W2_conv1, self.stride1))
        
        self.W2_conv2 = self.weight_variable([2, 4, 32, 64])
        self.h2_conv2 = tf.nn.relu(self.conv2d(self.h2_conv1, self.W2_conv2, self.stride2))
        
        self.W2_conv3 = self.weight_variable([2, 3, 64, 64])
        self.h2_conv3 = tf.nn.relu(self.conv2d(self.h2_conv2, self.W2_conv3, self.stride3))


        self.h1_flat = tf.reshape(self.h1_conv3, [-1, 5 * 7 *64])
        self.h2_flat = tf.reshape(self.h2_conv3, [-1, 5 * 7 *64])
        
        self.h_flat = tf.concat(1,[self.h1_flat, self.h2_flat, self.z])

        self.W_fc1 = self.weight_variable([7 + 5 * 7 * 64 + 5 * 7 * 64, 512])
        self.h_fc1 = tf.nn.relu(tf.matmul(self.h_flat, self.W_fc1))
        
        self.keep_prob = tf.placeholder(tf.float32)
        self.h_fc1_drop = tf.nn.dropout(self.h_fc1, self.keep_prob)
        
        self.W1_fc2 = self.weight_variable([512, 1])
        self.y1_conv = tf.matmul(self.h_fc1_drop, self.W1_fc2)
        self.cross_entropy1 = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(self.y1_conv, self.y1_))
        self.train_step1 = tf.train.AdamOptimizer(1e-4).minimize(self.cross_entropy1)
        self.correct_prediction1 = tf.equal(tf.argmax(self.y1_conv,1), tf.argmax(self.y1_,1))
        self.accuracy1 = tf.reduce_mean(tf.cast(self.correct_prediction1, tf.float32))
        
        self.W2_fc2 = self.weight_variable([512, 1])
        self.y2_conv = tf.matmul(self.h_fc1_drop, self.W2_fc2)
        self.cross_entropy2 = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(self.y2_conv, self.y2_))
        self.train_step2 = tf.train.AdamOptimizer(1e-4).minimize(self.cross_entropy2)
        self.correct_prediction2 = tf.equal(tf.argmax(self.y2_conv,1), tf.argmax(self.y2_,1))
        self.accuracy2 = tf.reduce_mean(tf.cast(self.correct_prediction2, tf.float32))
        
        self.W3_fc2 = self.weight_variable([512, 1])
        self.y3_conv = tf.matmul(self.h_fc1_drop, self.W3_fc2)
        self.cross_entropy3 = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(self.y3_conv, self.y3_))
        self.train_step3 = tf.train.AdamOptimizer(1e-4).minimize(self.cross_entropy3)
        self.correct_prediction3 = tf.equal(tf.argmax(self.y3_conv,1), tf.argmax(self.y3_,1))
        self.accuracy3 = tf.reduce_mean(tf.cast(self.correct_prediction3, tf.float32))
        
        self.W4_fc2 = self.weight_variable([512, 1])
        self.y4_conv = tf.matmul(self.h_fc1_drop, self.W4_fc2)
        self.cross_entropy4 = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(self.y4_conv, self.y4_))
        self.train_step4 = tf.train.AdamOptimizer(1e-4).minimize(self.cross_entropy4)
        self.correct_prediction4 = tf.equal(tf.argmax(self.y4_conv,1), tf.argmax(self.y4_,1))
        self.accuracy4 = tf.reduce_mean(tf.cast(self.correct_prediction4, tf.float32))
        
        self.W5_fc2 = self.weight_variable([512, 1])
        self.y5_conv = tf.matmul(self.h_fc1_drop, self.W5_fc2)
        self.cross_entropy5 = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(self.y5_conv, self.y5_))
        self.train_step5 = tf.train.AdamOptimizer(1e-4).minimize(self.cross_entropy5)
        self.correct_prediction5 = tf.equal(tf.argmax(self.y5_conv,1), tf.argmax(self.y5_,1))
        self.accuracy5 = tf.reduce_mean(tf.cast(self.correct_prediction5, tf.float32))
        
        self.W6_fc2 = self.weight_variable([512, 1])
        self.y6_conv = tf.matmul(self.h_fc1_drop, self.W6_fc2)
        self.cross_entropy6 = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(self.y6_conv, self.y6_))
        self.train_step6 = tf.train.AdamOptimizer(1e-4).minimize(self.cross_entropy6)
        self.correct_prediction6 = tf.equal(tf.argmax(self.y6_conv,1), tf.argmax(self.y6_,1))
        self.accuracy6 = tf.reduce_mean(tf.cast(self.correct_prediction6, tf.float32))
        
        self.sess.run(tf.global_variables_initializer())
        #self.sess.run(tf.initialize_all_variables())
        self.saver = tf.train.Saver()
        try:
            self.saver.restore(self.sess,self.file_name)
            print("Modelo cargado de archivo.")
        except:
            print("No hay un modelo para cargar.")
            save_path = self.saver.save(self.sess, self.file_name)
        self.QData = QLearningData()
        
