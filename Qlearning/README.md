# README #
Vilchis Salazar Jose Antonio

Seminario de Aprendizaje profundo aplicado a la robotica

Sobre busqueda de objetivos.

Mi seccion es crear una red neuronal que utilize la kobuki como agente de 
movimiento para encontrar atravez de la webcam de la laptop un objeto previamente
definido. 

La red se estructuro como una red Neuronal Q-Learning, la cual utiliza
una implementacion del algoritmo Qlearning para realizar el entrenamiento de la red.

Q Learning utiliza un espacio de distribucion dado por un estado actual y una orden,
el estado esta dado por el estado fisico de la kobuki, esto es que si se esta moviendo 
en cierta direccion, si esta detenida, o si el bumper se activo. La orden esta 
dada solamente por moverse al frente, hacia atras, a la izquierda o a la derecha,
si se queda quieto o si ya termino.

Q Learning a grandes rasgos intenta distribuir sobre el espacio de estados un valor,
el cual sera llamado premio, el cual dado un estado cual seria la orden que maximize
el premio dado.

La implementacion se realizo en tensorflow, usando una estructura similar al articulo
sobre redes neuronales de Qlearning para Atari la cual se explicara mas abajo.
El unico problema directo con tensorflow es que tenemos informacion incompleta, 
la implementacion de QLearning requiere que para cada estado se de el premio que 
daria cada una de las ordenes, sin embargo por la forma en la que se construyeron
los ejemplares de entrenamiento no es posible hacer esto, lo que se hizo fue que 
se asigno el premio solamente a la accion realizada, dejando un vector one-hot sobre
la orden que tiene en si misma el premio asignado, esto es un vector de ceros que
una de sus entradas es un numero distinto de cero el cual el indice representa la
orden dada y el contenido de tal entrada es el premio asignado por realizar tal accion
sobre el estado actual.
Dicho esto tenemos una implementacion incompleta dado que no podemos construir un 
vector completo para cada una de las acciones, por lo que aun no se como funcionara al
probarla, cabe mencionar que en las pruebas con los conjuntos de prueba y validacion dieron
resultados muy variados, desde ejemplares los cuales tuvieron un puntaje de 99% de 
presicion hasta otros que tenian puntajes de 0.5% de presicion, por lo que solamente
probando direrctamente con la kobuki tendremos resultados concluyentes.

Ademas es importante decir que el numero de ejemplares es reducido, 80 ejemplares
para el entrenamiento, 10 para prueba y 10 para validacion, por lo que la eficiencia
de la red sera baja dados los pocos ejemplares con los que trabajar.

La arquitectura de la red esta dada por 3 capas convolucionales, 1 capa oculta completa 
y una capa completa de salida, la ultima capa tiene una forma de (-1,6) que representan las
posibles opciones que puede tomar la red <arriba,abajo,izquierda,derecha,no moverse,termino>,
sin embargo tenemos el problema que el input de la red es parcial, esto es que nosotros ingresamos
solo los datos conocidos no todos los que la red necesita, por lo que el entrenamiento a pesar de 
que da una eficiencia de 95% solo son falsos positivos. Por este hecho se propuso modificar la 
arquitectura a manera de que la entrada y la salida solo contuvieran cada una de las salidas por separado,
esto es que en vez de tener un vector de longitud 6 tener 6 vectores de longitud 1.

Esta propuesta dio mejores resultados, sin embargo no fue por la arquitectura en si, si no porque
encontre un error diferente que habia desde la arquitectura anterior la cual tambien funciona: Habia un
metodo que obtenia el indice de un vector hot-one, pero solo funcionaba con vectores hot-one, si 
pasaba otro vector me daba el primer indice (siempre 0), por ello siempre se movia al frente.
Sin embargo ya se que hace algo.

DETALLES DE CODIGO.

__init__.py: Es un archivo que permite importar archivos.py del folder donde se encuentra __init__.py

captura.py: Modulo que funciona con ros para capturar los videos de entrenamiento de la kobuki, es el primero que se hizo.

clasificacion: Modulo de prueba, este imprime dos numeros con formato. Obtiene un ejemplar de prueba, y manda clasificar cada
uno de los cuadros del ejemplar con la red, primero imprime el valor esperado de tal cuadro del ejemplar. y luego imprime el 
de la clasificacion. El valor de un vector lo tomo como el indice de tal vector que contiene el numero mas alto.

data.py: Clase que representa la informacion que contiene un video. X1,X2,Z y Y

flat.py: Este archivo no se usa actualmente en el sistema, pero fue el primero en implementar QLearning.

prueba.py: Modulo que funciona con ros, para probar con la kobuki el funcionamiento de la red.

qLearningData.py: Modulo que lee los videos y los convierte a objetos de tipo data.py, y este tambien se 
encarga de devolver los ejemplares en orden dependiendo de si son de entrenamiento, prueba o validacion.

qLearning.py Modulo que contiene a la red neuronal, obtiene sus datos de qLearningData.py. Desde aqui
se entrena la red, y se prueba la eficiencia de validacion y de test. Sin embargo aun no he podido hacer que
devuelva el valor actual de porcentage de validacion porque tengo que "juntar" los vectores.

test.py: Modulo de pruebas, en este archivo pruebo el codigo general. Por ahora contiene solo el como entrena la red
y que obtenga el porcentage de prueba y validacion. No recomiendo ejecutarlo porque termina como en 8 horas.


NOTAS IMPORTANTES:

Aun no funciona el sistema, primero porque el entrenamiento es invalido, osea ya hace algo, pero no lo que deberia, porque
note un problema que se debio resolver hace mucho tiempo, y es que al leer los  videos no lee bien lo que esta leyendo:
Las ordenes estaban codificadas de cierta forma al grabar los videos y al leerlos por problemas de corrupcion de imagenes y
empaquetado se perdieron tales ordenes, entonces se intento recuperarlas, pero note con la ejecucion del archivo clasificacion.py
que el modulo QLearningData.py no puede corregir los errores iniciales, y peor aun, cada que lee un archivo este puede variar,
a veces al correr el archivo clasificacion.py manda un error de lectura que no puedo "cachar" pero lo notifico.
Entonces ahora queda volver a revisar la parte de OpenCv para recuperar la informacion de los archivos de video, pues
el entrenamiento que se hizo no refleja la realidad pues entreno con datos erroneos.

