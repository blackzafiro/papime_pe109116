
from data import Data
import numpy as np
np.set_printoptions(threshold=np.nan)
import cv2


class QLearningData:

    def __init__(self):
        #INDICES DE LOS BLOQUES - son inclusivos 
            #ENTERNAMIENTO 1-80: (80)
            #VALIDACION 81-90: (10)
            #PRUEBA 91-100: (10)
        self.test_init = 91
        self.test_limit = 100
        self.training_init = 1
        self.training_limit = 80
        self.validation_init = 81
        self.validation_limit = 90
        #Porcentage Gamma para Q-Learning
        self.gamma = 0.95
        #Porcentage para el castigo por usar el bumper
        self.reward = 0.05    
        #Son indices que indican el numero de los datos que siguen por usarse
        self.training = self.training_init
        self.test = self.test_init
        self.validation = self.validation_init   
        
    def correcion(self,X):
        X = X.flatten()
        if(X[0] < 128):
            X[0] = 0
        else:
            X[0] = 255
        if(X[1] < 128):
            X[1] = 0
        else:
            X[1] = 255
        if(X[2] < 128):
            X[2] = 0
        else:
            X[2] = 255
        #atras
        if X[0] == 255 and X[1] == 255 and X[2] ==255:
            X = np.array([0,255,0,0,0,0])
        #izquierda
        elif X[0] == 255:
            X = np.array([0,0,255,0,0,0])
        #adelante
        elif X[1] == 255:
            X = np.array([255,0,0,0,0,0])
        #derecha
        elif X[2] == 255:
            X = np.array([0,0,0,255,0,0])
        #quieto
        else:
            X = np.array([0,0,0,0,255,0])
        return X
        
    #Lee un solo ejemplar de los videos, el numero n
    def leer(self, n, prize = True):
        name = 'output' + str(n)
        
        #Cuadro de panorama completo
        X1 = []
        #Cuadro de panorama corto
        X2 = []
        #Bias <estado,bumper>
        Z = []
        #Resultado <orden,premio>
        Y = []
        
        try:
            cap = cv2.VideoCapture(name + '.avi')
            if not cap.isOpened():
                print("No existe el archivo " + name + ".")
                data = Data(X1,X2,Z,Y)
                return data
            ret, frame = cap.read()
            while(True):
                if not ret:
                    break
                array = np.array(frame,copy = True)
                array = np.asfarray(array,dtype='float32')
                #Define el cuadro completo
                aux = array[:,0:84,:].flatten()
                X1.append(aux)
                #Define el cuadro corto
                aux = array[:,84:84*2,:].flatten()
                X2.append(aux)
                
                #rectifica los valores de "estado"
                estado = array[0,-4,:]
                estado = self.correcion(estado)

                #vector de 5 one hot para la orden
                orden = array[0,-2,:]
                orden = self.correcion(orden)
                
                #vector de 1 para el bumper
                if estado[1] == 255:
                    bumper = np.array([255])
                else:
                    bumper = np.array([0])
                #vector de 1 para el premio
                premio = np.array([0])
                #escribir arreglo
                
                #volver a leer otro cuadro
                ret, frame = cap.read()
                
                #para revisar si ya es el el ultimo cuadro
                if not ret:
                    if(prize):
                        premio[0] = 255
                    else:
                        premio[0] = -255
                    #Esta es la orden/estado de que ya termino
                    orden = np.array([0,0,0,0,0,255])
                    estado = np.array([0,0,0,0,0,255])
                    
                aux = np.hstack((estado,bumper))                  
                #meter el arreglo en Z
                Z.append(aux)
                aux = np.hstack((orden,premio))
                Y.append(aux)
                #print(bumper)
            #break
            
        except ValueError:
            print("error?")
        
        X1 = np.array(X1,dtype='float32')
        X2 = np.array(X2,dtype='float32')
        Z = np.array(Z,dtype='float32')
        #Qlearning
        Y = np.array(Y,dtype='float32')
        r = np.array(Y.shape)
        r[0] = r[0] - 2
        r[1] = r[1] - 1
        while(r[0]>=0):
            Y[r[0],r[1]] = - self.reward*Z[r[0],r[1]] + self.gamma*Y[r[0]+1,r[1]]
            r[0] = r[0] - 1
        ##SUPER FUMADA
        R = Y[:,-1]
        R =  np.vstack((R,R,R,R,R,R)).T/255.0
        Y = Y[:,:-1] * R

        #"Normalizamos" para que los valores esten entre 0 y 1
        X1 =X1 / 255.0
        X2 = X2 / 255.0
        Z = Z / 255.0
        Y = Y / 255.0
        
        print("El Ejemplar " + str(n) + " ha sido cargado a memoria.")
        data = Data(X1,X2,Z,Y)
        return data
      
    def getNextTrainingData(self):
        self.training = self.training + 1
        if(self.training == self.training_limit):
            self.training = self.training_init
        data = self.leer(self.training)
        return data
        
    def getNextTestData(self):
        self.test = self.test + 1
        if(self.test == self.test_limit):
            self.test = self.test_init
        data = self.leer(self.test)
        return data
        
    def getNextValidationData(self):
        self.validation = self.validation + 1
        if(self.validation == self.validation_limit):
            self.validation = self.validation_init
        data = self.leer(self.validation)
        return data
        
    
