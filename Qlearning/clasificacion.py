from qLearningData import QLearningData
from qLearning import QLearning
from data import Data
import numpy as np
import tensorflow as tf

if __name__ == "__main__":
    qLearning = QLearning()
    qData = QLearningData()
    
    data = qData.getNextTestData()
    size = data.X1.shape[0]
    for i in range(size):
        X1 = data.X1[i]
        X2 = data.X2[i]
        Z = data.Z[i]
        Y = data.Y[i]
        data_set = Data(X1,X2,Z,Y)
        clasificacion = qLearning.clasification(data_set)
        Y_ = qLearning.getIndex(qLearning.reshape(Y))
        print(str(Y_) + "  " + str(clasificacion))
