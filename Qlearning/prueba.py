#!/usr/bin/env python

from qLearningData import QLearningData
from qLearning import QLearning
from data import Data
import rospy
import numpy as np
import cv2
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3
from kobuki_msgs.msg import SensorState

b = 0
cX = 848
cY = 480
X_ = 84
Y_ = 48
alpha = 0.1
betha = 0.5   
nX = (cX / 4)
nY = (cY / 4)
estado = np.zeros((Y_,1,3),dtype="uint8")
bumper = np.zeros((Y_,1,3),dtype="uint8")
orden = np.zeros((Y_,1,3),dtype="uint8")
premio = np.zeros((Y_,1,3),dtype="uint8")
pub = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size = 10)
rospy.init_node('visionPrueba', anonymous=True) 

qData = QLearningData()
qLearning = QLearning()

def train(interrupt, forcePrize, inpt):
    
    try:
        data = qData(inpt,interrupt)
        print("Ejemplar " + str(inpt) + " enternado.")
        qLearning.train(data)
    except:
        print("Error en entrenamiento, ejemplar: " + str(self.QData.training))
    qLearning.save()

def cambio(data):
    global b
    
    if data.left_pwm == 0 and data.right_pwm == 0:
        b = 0
        estado[:,:,0] = 0
        estado[:,:,1] = 0
        estado[:,:,2] = 0
        
    else:
        if b != 1:
            estado[:,:,0] = orden[:,:,0]
            estado[:,:,1] = orden[:,:,1]
            estado[:,:,2] = orden[:,:,2]
    if data.bumper == 0:
        bumper[:,:,0] = 0
        bumper[:,:,1] = 0
        bumper[:,:,2] = 0
    else:
        b = 1
        bumper[:,:,0] = 255
        bumper[:,:,1] = 255
        bumper[:,:,2] = 255
        estado[:,:,0] = 255
        estado[:,:,1] = 255
        estado[:,:,2] = 255
        premio[:,:,0] = 127
        premio[:,:,1] = 127
        premio[:,:,2] = 127
        print("vv")
        pub.publish(Twist(Vector3(-alpha,0,0),Vector3(0,0,0)))

def prueba():
    global Y_
    interrupt = False
    forcePrize = False
    
    
    rospy.Subscriber("/mobile_base/sensors/core",SensorState,cambio)
    
    ffile = open('file.txt','r')
    st = ffile.readline()
    ffile.close()
    name = 'output' + st
    inpt = int(st)
    inpt = inpt + 1
    ffile = open('file.txt','w+')
    ffile.write(str(inpt))
    ffile.close()
    #cap = cv2.VideoCapture('output.avi')
    cap = cv2.VideoCapture(0)
    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'MJPG')
    
    out = cv2.VideoWriter(name + '.avi',fourcc, 20.0, (2*X_+4,Y_))

    while(True):
        premio[:,:,0] = 0
        premio[:,:,1] = 0
        premio[:,:,2] = 0
        ret, frame = cap.read()
        if not ret:
            break
        key = cv2.waitKey(10)
        if key == ord('q'): 
            interrupt = True
            break
            
        if key == ord('r'): 
            print("p")
            forcePrize = True
            premio[:,:,0] = 255
            premio[:,:,1] = 255
            premio[:,:,2] = 255 
            break; 
            
        cachito = frame[nY:nY * 3,nX:nX * 3,:]
        frame = cv2.resize(frame,(X_,Y_))
        cachito = cv2.resize(cachito,(X_,Y_))
            
        X1 = frame.flatten()
        X2 = cachito.flatten()
        Z = qData.correcion(estado)
        if(bumper[0,0,0] == 255 and bumper[0,0,1] == 255 and bumper[0,0,2] == 255):
            aux = np.array([255])
        else:
            aux = np.array([0])
        Z = np.hstack((Z,aux))
        Y = np.array(np.array([0,0,0,0,0,0]))
        data = Data(X1,X2,Z,Y)
        
        k = qLearning.clasification(data)
        print(k)
        if k == 2: 
            print("<")
            pub.publish(Twist(Vector3(0,0,0),Vector3(0,0,betha)))
            orden[:,:,0] = 255
            orden[:,:,1] = 0
            orden[:,:,2] = 0 

        if k == 1: 
            print("v")
            pub.publish(Twist(Vector3(-alpha,0,0),Vector3(0,0,0)))
            orden[:,:,0] = 255
            orden[:,:,1] = 255
            orden[:,:,2] = 255

        if k == 3: 
            print(">")
            pub.publish(Twist(Vector3(0,0,0),Vector3(0,0,-betha)))
            orden[:,:,0] = 0
            orden[:,:,1] = 0
            orden[:,:,2] = 255 
 
        if k == 0: 
            print("^")
            pub.publish(Twist(Vector3(alpha,0,0),Vector3(0,0,0)))
            orden[:,:,0] = 0
            orden[:,:,1] = 255
            orden[:,:,2] = 0  
            
        if k == 4: 
            print("o")
            pub.publish(Twist(Vector3(0,0,0),Vector3(0,0,0)))
            orden[:,:,0] = 0
            orden[:,:,1] = 0
            orden[:,:,2] = 0  
        
        if k == 5: 
            print("p")
            pub.publish(Twist(Vector3(0,0,0),Vector3(0,0,0)))
            premio[:,:,0] = 255
            premio[:,:,1] = 255
            premio[:,:,2] = 255  
        
        
        frame = np.hstack((frame,cachito,estado,bumper,orden,premio))

        cv2.imshow('frame', frame)
        out.write(frame)

    cap.release()
    out.release()
    cv2.destroyAllWindows()
    #train(interrupt, forcePrize, inpt-1)

if __name__ == '__main__':
    prueba()
