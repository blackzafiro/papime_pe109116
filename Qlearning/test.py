
from qLearningData import QLearningData
from qLearning import QLearning
from data import Data
import numpy as np
import tensorflow as tf


if __name__ == "__main__":
    qLearning = QLearning()
    qData = QLearningData()
    for i in range(10):
        qLearning.training()
    
    validate = qLearning.validating()
    test = qLearning.testing()
    tf.Print(test)
    tf.Print(validate)
    
